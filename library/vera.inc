.const VERAREG   = $9f20
// Note the doc (0.8) has MID and HI reversed?
// Not sure which is right
.const VERALO    = VERAREG + 0
.const VERAMID   = VERAREG + 1
.const VERAHI    = VERAREG + 2
.const VERADAT   = VERAREG + 3
.const VERADAT2  = VERAREG + 4
.const VERACTL   = VERAREG + 5
.const VERAIEN   = VERAREG + 6
.const VERAISR   = VERAREG + 7

.const VREG_CMP  = $F0000
.const VREG_PAL  = $F1000
.const VREG_LAY1 = $F2000
.const VREG_LAY2 = $F3000
.const VREG_SPR  = $F4000
.const VREG_SPRD = $F5000

.const AUTO_INC_1 = $100000

.macro vset (addr) {
	lda #(addr >> 16) | $10
	sta VERAHI
	lda #(addr >> 8)
	sta VERAMID
	lda #(addr)
	sta VERALO
}

.macro vstore (addr) {
	pha
	vset(addr)
	pla
	sta VERADAT
}

.macro vload (addr) {
	vset(addr)
	lda VERADAT
}

.macro sprset (offset) {
	lda #(VREG_SPRD >> 16) | $10
	sta VERAHI
	txa
	lsr
	lsr
	lsr
	lsr
	lsr
	clc
	adc #(VREG_SPRD + offset >> 8)
	sta VERAMID
	txa
	asl
	asl
	asl
	clc
	adc #(VREG_SPRD + offset)
	sta VERALO
}

.macro sprload (offset) {
	sprset(offset)
	lda VERADAT
}

.macro sprstore(offset) {
	pha
	sprset(offset)
	pla
	sta VERADAT
}

.macro video_init() {
	lda #0
	sta VERACTL // set ADDR1 active
	sta VERAMID
	lda #$1F    // $40040
	sta VERAHI
	lda #$00
	sta VERALO
	lda #1
	sta VERADAT // VGA output
}
