:BasicUpstart2(setup) 

.const CINT = $ff81
.const CLRCHN = $ffcc
.const CHROUT = $ffd2
.const PLOT = $fff0
.const STRING = "HELLO WORLD!!!"

/* Macros */
// Move the cursor to given X,Y coordinates
.macro GotoCoords (x,y) {
        ldx     #x
        ldy     #y
        clc 
        jsr     PLOT
}

// Print string from given block
// Block should be something like: 
// msg:    .text  STRING
//         .byte 0 
.macro PrintString (msg) {
    loop:   lda    msg,x
            jsr    CHROUT
            inx
            bne    loop
}

setup:
        // Some Screen Setup
        jsr     CLRCHN  // Reset I/O
        jsr     CINT    // Clear screen

        // Goto 20,20 decimal  on screen
        GotoCoords(20,20)

        // Reset registers
        ldx     #$0
        ldy     #$0

main:   PrintString(msg)

final:  GotoCoords(60,60)
        rts

msg:    .text  STRING   
        .byte 0         
