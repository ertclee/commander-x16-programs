/* Print an 8-bit hex number */

#import "library/constants.inc"
#import "library/printhex.inc"

:BasicUpstart2(setup)

// Number to print
setup:
         lda #$f4
         jsr printhex
         rts
