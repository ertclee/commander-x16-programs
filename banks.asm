/* Ask for two numbers, add them, print result */

#import "library/macros.inc"
#import "library/constants.inc"
#import "library/printhex.inc"
// #import "library/chartonum.inc"

.const SELECT_BANK = $9f61
.const TEST_ADDR = $a000


:BasicUpstart2(main)

main:
  // Store stuff in different pages of memory
  // Switch to bank f0
  lda #$f0
  sta SELECT_BANK
  lda #$af          // Store af into test address
  sta TEST_ADDR
  // Swtich to bank 0a
  lda #$0a
  sta SELECT_BANK
  lda #$50          // Store 50 into test address
  sta TEST_ADDR

results:
  lda #$f0
  sta SELECT_BANK
  lda TEST_ADDR
  jsr printhex

  lda #$0a
  sta SELECT_BANK
  lda TEST_ADDR
  jsr printhex

  rts
